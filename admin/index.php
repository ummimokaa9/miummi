<?php




session_start();


if (!$_SESSION["login"]){
  header("Location:../meetup/index.php ");
  exit;
}
// Database connection parameters
$servername = "localhost";
$username = "root";
$password = ""; // If you have set a password for the 'root' user, enter it here
$dbname = "ummi";

// Create a new database connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check the connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Retrieve data from the database
$sql = "SELECT id,nama, pesan, ppp, kkk FROM hasil";
$result = $conn->query($sql);

?>






<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>SpringTime</title>
<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="css/style.css" type="text/css" media="all" />
</head>
<body>
<!-- Header -->

<!-- End Header -->
<!-- Container -->
<div id="container">
  <div class="shell">
    <!-- Small Nav -->
    <div class="small-nav"> <a href="keluar.php">logout</a>  </div>
    <!-- End Small Nav -->
    <!-- Message OK -->
  
    <!-- End Message OK -->
    <!-- Message Error -->
  
    <!-- End Message Error -->
    <br />
    <!-- Main -->
    <div id="main">
      <div class="cl">&nbsp;</div>
      <!-- Content -->
      <div id="content">
        <!-- Box -->
        <div class="box">
          <!-- Box Head -->
          <div class="box-head">
            <h2 class="left">Current Articles</h2>
            
          </div>
          <!-- End Box Head -->
          <!-- Table -->
          <div class="table">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <th width="13"><input type="checkbox" class="checkbox" /></th>
                <th>Title</th>
                <th>Date</th>
                <th>Added by</th>
                <th width="110" class="ac">Content Control</th>
              </tr>
             <?php



// Check if there are any rows returned
if ($result->num_rows > 0) {
  // Output data of each row
  while ($row = $result->fetch_assoc()) {?>
              <tr class="odd">
                <td><input type="checkbox" class="checkbox" /></td>
                <td><h3><?php echo $row["nama"] ?></h3></td>
                <td><?php echo $row["kkk"] ?></td>
                <td><?php echo $row["ppp"] ?></td>
                <td><a href="hapus.php?delete=<?php echo $row["id"] ?>" class="ico del">Delete</a></td>
              </tr>
           <?php


}
} else {
    echo "No results found.";
}

// Close the database connection
$conn->close();
?>
            </table>
           
            <!-- End Pagging -->
          </div>
          <!-- Table -->
        </div>
        <!-- End Box -->
        <!-- Box -->
        <div class="box">
          <!-- Box Head -->
          <div class="box-head">
            <h2>Add New Article</h2>
          </div>
          <!-- End Box Head -->
          <form action="tambah.php" method="post">
            <!-- Form -->
            <div class="form">
              <p> <span class="req">max 100 symbols</span>
                <label>Article Title <span>(Required Field)</span></label>
                <input type="text" class="field size1"  name="nama"/>
              </p>
         
              <p> <span class="req">max 100 symbols</span>
                <label>Content <span>(Required Field)</span></label>
                <textarea class="field size1" rows="10" cols="30" name="pesan"></textarea>
              </p>
            </div>
            <!-- End Form -->
            <!-- Form Buttons -->
            <div class="buttons">
         
              <input type="submit" class="button" value="submit" name="submit"/>
            </div>
            <!-- End Form Buttons -->
          </form>
        </div>
        <!-- End Box -->
      </div>
      <!-- End Content -->
      <!-- Sidebar -->
     
      <!-- End Sidebar -->
      <div class="cl">&nbsp;</div>
    </div>
    <!-- Main -->
  </div>
</div>
<!-- End Container -->
<!-- Footer -->
<div id="footer">
  <div class="shell"> <span class="left">&copy; 2010 - CompanyName</span> <span class="right"> Design by <a href="http://chocotemplates.com">Chocotemplates.com</a> </span> </div>
</div>
<!-- End Footer -->
</body>
</html>
