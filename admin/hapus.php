<?php
// Database connection parameters
$servername = "localhost";
$username = "root";
$password = ""; // If you have set a password for the 'root' user, enter it here
$dbname = "ummi";

// Create a new database connection
$conn = new mysqli($servername, $username, $password, $dbname);

// Check the connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

// Check if the form is submitted for deleting a record
if(isset($_GET['delete'])) {
    // Retrieve the ID of the record to be deleted
    $id = $_GET['delete'];

    // Prepare the SQL statement to delete the record
    $deleteSql = "DELETE FROM hasil WHERE id = '$id'";

    // Execute the SQL statement
    if ($conn->query($deleteSql) === TRUE) {
        header("Location:index.php ");
        exit;
    } else {
        echo "Error deleting record: " . $conn->error;
    }
}
